import urllib
import re
import pymongo
import requests
import json

def get_user(search):
    bit_url = 'https://bitbucket.org/repo/all/1?name=' + search
    bit_html = urllib.request.urlopen(bit_url)
    bit_text = bit_html.read()
    find_text = bit_text.decode('utf-8')
    result_regex = 'Showing(.+?)results'
    all = re.findall(re.compile(result_regex),find_text)
    Number = int(all[0])

    print(Number)
    for i in range(1,int(Number/10)+1):
        bit_url = 'https://bitbucket.org/repo/all/' +str(i) + '?name=' + search
        bit_html = urllib.request.urlopen(bit_url)
        bit_text = bit_html.read()
        find_text = bit_text.decode('utf-8')
        users_regex = 'href=\"/(.+?)/'
        all_users = re.findall(re.compile(users_regex),find_text)
        user_set = set()
        for user in all_users:
            user_set.add(user)
        for user in user_set:
            yield user

def user_add(search):
    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['Bitbucket']
    collection_name = 'profiles_bitbucket'

   # if collection_name in mng_db.collection_names():
    #    mng_db.drop_collection(collection_name)

    col = mng_db[collection_name]

    for user in get_user(search):
        if col.find({'_id':user}).count()==0:
            url = 'https://api.bitbucket.org/1.0/users/' + user
            json_obj = requests.get(url)
            print(json_obj)
            if json_obj.status_code == 404:
                data = {}
                data['_id'] = user
                data['valid'] = False
                print('not valid ', user)
                col.insert_one(data)
            else :
                data = json_obj.json()
                print(data['user']['username'])
                data['_id'] = user
                col.insert_one(data)


user_add('java')
